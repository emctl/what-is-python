# what-is-python

Companion repo to "what is python" blog post from https://emctl.gitlab.io/ 

## Getting started

### Prerequisites 
- [ ] Install python3
- [ ] Install pytest

### How to run

```python
cd to <this repo>
chmod +x *.py

# to test
pytest envvar-hw.py

# to run
python3 envvar-hw.py
python3 simple-hw.py

# to test env var
export TEST="<your string>"
python3 envvar-hw.py
