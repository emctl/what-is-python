#!/usr/bin/env python3

# How to use environment variables
from os import environ

def hello_world():
    # If you use the Env Var, hello + var
    if environ.get('TEST'):
        return (f'hello, {environ["TEST"]}!')
    else:
        return "hello, world!"

### TESTING ###

# Standard test
def test_hello_world():
    if environ.get('TEST'):
        pre=environ.get('TEST')
        del environ['TEST']
        assert hello_world() == "hello, world!"
        environ['TEST']=pre
    else:
        assert hello_world() == "hello, world!"


# Env var test
def test_hello_world_var():
    if environ.get('TEST'):
        pre=environ.get('TEST')
        environ['TEST']="testing"
        assert hello_world() == "hello, testing!"
        environ['TEST']=pre
    else:
        environ['TEST']="testing"
        assert hello_world() == "hello, testing!"